package org.restaurant.common.thread;

public class AutoresetEvent {
    private final Object monitor = new Object();
    private volatile boolean isOpen = false;

    public AutoresetEvent(boolean open)
    {
        isOpen = open;
    }

    public void waitOne() throws InterruptedException {
        synchronized (monitor) {
            while (!isOpen) {
                monitor.wait();
            }
            isOpen = false;
        }
    }

    public boolean waitOne(long timeout) throws InterruptedException {
        synchronized (monitor) {
            try {
                long t = System.currentTimeMillis();
                while (!isOpen) {
                    monitor.wait(timeout);
                    if (System.currentTimeMillis() - t >= timeout) {
                        break;
                    }
                }
                return isOpen;
            } finally {
                isOpen = false;
            }
        }
    }

    public void set() {
        synchronized (monitor) {
            isOpen = true;
            monitor.notifyAll();
        }
    }

    public void reset()
    {
        synchronized (monitor) {
            isOpen = false;
        }
    }
}
