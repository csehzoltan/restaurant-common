package org.restaurant.common.helpers;

public class UnitException extends Exception {

	private static final long serialVersionUID = -3246069210424911399L;

	public UnitException() {
		super();
	}

	public UnitException(String message) {
		super(message);
	}

	public UnitException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnitException(Throwable cause) {
		super(cause);
	}
}