package org.restaurant.common.helpers;

import java.math.BigDecimal;

public class NumberHelper {
	/**
	 * Double értéket ad vissza a kért pontossággal, féltől felfelé kerekít.
	 * @param value A double érték amin a pontosságot szeretnénk megadni.
	 * @param scale A double érték pontossága.
	 * @return Visszatér egy skálázott double számmal.
	 */
	public static double scaleDouble(double value, int scale) {
		return BigDecimal.valueOf(value).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/**
	 * A megadott egész számot megpróbálja double értékké konvertálni, ha nem sikerül, akkor az alapértelmezett értékkel tér vissza.
	 * @param value Egész szám amit double értékké szeretnénk konvertálni.
	 * @param def Alapértelmezett érték, amit visszaad, ha a konverzió sikertelen.
	 * @return Double érték, amit integerből konvertál.
	 */
	public static Double parseDoubleParam(Integer value, Double def) {
		try {
			return value.doubleValue();
		} catch (NumberFormatException | NullPointerException e) {
			return def;
		}
	}
	
}
