package org.restaurant.common.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateHelper {
	/**
	 * Visszatér a mai dátumtól a megadott számmal korábbi dátummal.
	 * @param diffDays Hány nappal ezelőtti dátumot szeretnénk megadni (egész szám).
	 * @return String X nappal ezelőtti dátumot adja vissza. Minta dátum: 2018\11\20\
	 */
	public static String getToday(int diffDays) {	
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1 * diffDays);
		
		String year = String.valueOf(cal.get(Calendar.YEAR));
		String month = String.valueOf(cal.get(Calendar.MONTH) + 1); 
		String day = String.valueOf(cal.get(Calendar.DATE));
		
		if (month.length() != 2) {
			month = "0" + month;
		}
		
		if (day.length() != 2) {
			day = "0" + day;
		}
		
		return year + "\\" + month + "\\" + day + "\\";
	}
	
	/**
	 * Adott dátumot megformázza a megadott formázási stílusra.
	 * @param date Megadott dátum.
	 * @param fromFormat Eredeti formázási stílus.
	 * @param toFormat Elvárt (eredmény) formázási stílus.
	 * @param locale Földrajzi régió.
	 * @return visszatér a dátum az elvárt formázással.
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static String format(String date, String fromFormat, String toFormat, Locale locale) throws ParseException {
		SimpleDateFormat dfOld = new SimpleDateFormat(fromFormat, locale);
		SimpleDateFormat dfNew = new SimpleDateFormat(toFormat, locale);

		String resultDate = null;
		if (date != null) {
			resultDate = dfNew.format(dfOld.parse(date));
		}
		return resultDate;
	}
}
