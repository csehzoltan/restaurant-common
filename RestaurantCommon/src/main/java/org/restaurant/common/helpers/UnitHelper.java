package org.restaurant.common.helpers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UnitHelper {
	
	public static double unitConversion(double value, String from, String to) throws UnitException {

		Map<String, Integer> mass = new HashMap<>();
		mass.put("mg", 1);
		mass.put("g", 2);
		mass.put("kg", 3);

		if (isVolume(from, to, mass)) {
			return volumeUnitConversion(value, from, to);
		} else {
			return massUnitConversion(value, from, to, mass);
		}
	}

	private static boolean isVolume(String from, String to, Map<String, Integer> mass) throws UnitException {
		boolean isVolume;
		Set<String> volumes = new HashSet<String>();
		volumes.add("ml");
		volumes.add("dl");
		volumes.add("l");

		if (volumes.contains(from) && volumes.contains(to)) {
			isVolume = true;
		} else if (mass.get(from) != null && mass.get(to) != null) {
			isVolume = false;
		} else
			throw new UnitException("Wrong unit type!");
		return isVolume;
	}

	private static double massUnitConversion(double value, String from, String to, Map<String, Integer> mass) {
		return value * Math.pow(1000, (double) mass.get(from) - mass.get(to));
	}

	private static double volumeUnitConversion(double value, String from, String to) {
		if ("ml".equals(from) && "dl".equals(to)) {
			return value / 100;
		} else if ("dl".equals(from) && "ml".equals(to)) {
			return value * 100;
		} else if ("dl".equals(from) && "l".equals(to)) {
			return value / 10;
		} else if ("l".equals(from) && "dl".equals(to)) {
			return value * 10;
		} else if ("l".equals(from) && "ml".equals(to)) {
			return value * 1000;
		} else if ("ml".equals(from) && "l".equals(to)) {
			return value / 1000;
		} else
			return value;
	}
}
