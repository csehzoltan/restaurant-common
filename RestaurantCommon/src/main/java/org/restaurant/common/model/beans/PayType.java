package org.restaurant.common.model.beans;

public enum PayType {
	ONEPAYS(1, "One pays"), 
	SEPARATE(2, "Separate");
	
	private int id;
	private String display;

	PayType(int id, String display) {
		this.id = id;
		this.display = display;
	}
	
	public static PayType getById(int id) {
		for (PayType e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static PayType getByValue(String display) {
		for (PayType e : values()) {
			if (e.display.equalsIgnoreCase(display)) {
				return e;
			}
		}
		return null;
	}

	public String getDisplay() {
		return display;
	}

	public int getId() {
		return id;
	}
}
