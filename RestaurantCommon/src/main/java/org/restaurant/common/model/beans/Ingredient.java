package org.restaurant.common.model.beans;

public class Ingredient extends SimpleBean {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Double quantity;
	private Double alertLimit;
	private Unit unit;
	private byte[] ingredientImg;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getQuantity() {
		return quantity;
	}
	
	public Double getAlertLimit() {
		return alertLimit;
	}

	public void setAlertLimit(Double alertLimit) {
		this.alertLimit = alertLimit;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	
	public byte[] getIngredientImg() {
		return ingredientImg;
	}

	public void setIngredientImg(byte[] ingredientImg) {
		this.ingredientImg = ingredientImg;
	}
	
}
