package org.restaurant.common.model.beans;

import java.io.Serializable;

public class CoordXY  implements Serializable {
	
	private static final long serialVersionUID = 6414148199334838758L;
	
	private String xText;
	private String yText;
	private Double value;
	
	public String getxText() {
		return xText;
	}

	public void setxText(String xText) {
		this.xText = xText;
	}

	public String getyText() {
		return yText;
	}

	public void setyText(String yText) {
		this.yText = yText;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
