package org.restaurant.common.model.beans;

import java.util.Date;
import java.util.List;

public class ProductRequest extends SimpleBean {

	private static final long serialVersionUID = -2068735131635325814L;
	
	private User requestedBy;
	private Date requestedAt;
	private Table table;
	private User waiter;
	private ProductRequestState state;
	private List<ProductOrder> productOrders;
	private PayType payType;

	public Date getRequestedAt() {
		return requestedAt;
	}

	public User getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(User requestedBy) {
		this.requestedBy = requestedBy;
	}

	public User getWaiter() {
		return waiter;
	}

	public void setWaiter(User waiter) {
		this.waiter = waiter;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}

	public ProductRequestState getState() {
		return state;
	}

	public void setState(ProductRequestState state) {
		this.state = state;
	}
	
	public List<ProductOrder> getProductOrders() {
		return productOrders;
	}

	public void setProductOrders(List<ProductOrder> productOrders) {
		this.productOrders = productOrders;
	}
	
	public PayType getPayType() {
		return payType;
	}

	public void setPayType(PayType payType) {
		this.payType = payType;
	}
}
