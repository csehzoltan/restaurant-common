package org.restaurant.common.model.beans;

import java.util.Comparator;

public class CoordXYYNumberComparator implements Comparator<CoordXY>{

	@Override
	public int compare(CoordXY o1, CoordXY o2) {
		return Integer.parseInt(o1.getyText()) - Integer.parseInt(o2.getyText());
	}

}
