package org.restaurant.common.model.beans;

public class ProductPrice extends SimpleBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8530815018698425019L;
	private Product product;
	private Double amount;
	private Unit unit;
	private double price;
	private Size size;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}
	
	public String getDisplayText() {
		String priceText = "";
		if(getAmount() != null) {
			priceText += getAmount() + getUnit().getValue() + " - $" + getPrice(); 
		} else if(getSize() != null) {
			priceText += getSize().getDisplay() + " - $" + getPrice();
		}
		
		return priceText;
	}
	
	/**
	 * Megadja a rendelés mennyiségét, ami lehet mértékegység, vagy méret (kicsi, közepes, nagy).
	 * @return Visszatér a rendelés mennyiségével, amennyiben ki van töltve a mennyiség, visszatér a mértékegységgel, amennyiben 
	 * pedig a méret van megadva, visszatér a mérettel, ami lehet kicsi, közepes, nagy.
	 */
	public String getAmountDisplay() {
		String result = "";
		if (getAmount() != null) {
			result += getAmount() + getUnit().getValue();
		} else if (getSize() != null) {
			result += getSize().getDisplay();
		}
		return result;
	}
}
