package org.restaurant.common.model.beans;

public enum Size {
	SMALL(1, "SM"), MEDIUM(2, "MD"), LARGE(3, "LG");

	private int id;
	private String display;

	Size(int id, String display) {
		this.id = id;
		this.display = display;
	}

	public static Size getById(int id) {
		for (Size e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static Size getByValue(String display) {
		for (Size e : values()) {
			if (e.display.equalsIgnoreCase(display)) {
				return e;
			}
		}
		return null;
	}

	public String getDisplay() {
		return display;
	}

	public int getId() {
		return id;
	}

}
