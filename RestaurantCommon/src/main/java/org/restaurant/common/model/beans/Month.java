package org.restaurant.common.model.beans;

public enum Month {
	JANUARY(1, "January"),
	FEBRUARY(2, "February"),
	MARCH(3, "March"),
	APRIL(4, "April"),
	MAY(5, "May"),
	JUNE(6, "June"),
	JULY(7, "July"),
	AUGUST(8, "August"),
	SEPTEMBER(9, "September"),
	OCTOBER(10, "October"),
	NOVEMBER(11, "November"),
	DECEMBER(12, "December");
	
	private final int no;
	private final String name;
	
	private Month(int no, String name) {
		this.no = no;
		this.name = name;
	}

	public static Month getByNo(int no) {
		for (Month e : values()) {
			if (e.no == no) {
				return e;
			}
		}
		return null;
	}

	public static Month getByName(String name) {
		for (Month e : values()) {
			if (e.getName().equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNo() {
		return no;
	}
}
