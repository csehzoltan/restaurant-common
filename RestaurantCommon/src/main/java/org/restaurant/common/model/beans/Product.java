package org.restaurant.common.model.beans;

import java.util.List;

public class Product extends SimpleBean {

	private static final long serialVersionUID = -8822386139687159652L;
	private String name;
	private ProductCategory category;
	private String description;
	private byte[] img;
	private List<ProductPrice> prices;
	private List<Ingredient> ingredients;
	
	public Product() {}
	
	public Product(Integer id, String name, ProductCategory category, String description, byte[] img, List<ProductPrice> prices, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.description = description;
		this.img = img;
		this.prices = prices;
		this.ingredients = ingredients;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public ProductCategory getCategory() {
		return category;
	}

	public void setCategory(ProductCategory category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public List<ProductPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<ProductPrice> prices) {
		this.prices = prices;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

}
