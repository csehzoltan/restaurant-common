package org.restaurant.common.model.beans;

public enum ProductOrderState {
	INPROGRESS(1, "In Progress"), DONE(2, "Done");

	private int id;
	private String display;

	ProductOrderState(int id, String display) {
		this.id = id;
		this.display = display;
	}

	public static ProductOrderState getById(int id) {
		for (ProductOrderState e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static ProductOrderState getByValue(String display) {
		for (ProductOrderState e : values()) {
			if (e.display.equalsIgnoreCase(display)) {
				return e;
			}
		}
		return null;
	}

	public String getDisplay() {
		return display;
	}

	public int getId() {
		return id;
	}
}
