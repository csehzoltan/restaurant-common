package org.restaurant.common.model.beans;

public enum Unit {

	MG(1, "mg", "mass"),
	G(2, "g", "mass"),
	KG(3, "kg", "mass"),
	ML(4, "ml", "volume"),
	DL(5, "dl", "volume"),
	L(6, "l", "volume");

	private final int id;
	private final String value;
	private final String type;

	Unit(int id, String value, String type) {
		this.id = id;
		this.value = value;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public static Unit getById(int id) {
		for (Unit e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static Unit getByValue(String value) {
		for (Unit e : values()) {
			if (e.value.equals(value)) {
				return e;
			}
		}
		return null;
	}

	public String getValue() {
		return value;
	}
	
	public String getType() {
		return type;
	}
}
