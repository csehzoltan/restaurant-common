package org.restaurant.common.model.beans;

import java.util.Date;

public class Guestbook extends SimpleBean {

	private static final long serialVersionUID = 219150937271722323L;
	
	private User user;
	private String remark;
	private Integer foodRating;
	private Integer priceRating;
	private Integer staffRating;
	private Date arrivedAt;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getFoodRating() {
		return foodRating;
	}

	public void setFoodRating(Integer foodRating) {
		this.foodRating = foodRating;
	}

	public Integer getPriceRating() {
		return priceRating;
	}

	public void setPriceRating(Integer priceRating) {
		this.priceRating = priceRating;
	}

	public Integer getStaffRating() {
		return staffRating;
	}

	public void setStaffRating(Integer staffRating) {
		this.staffRating = staffRating;
	}
	
	public Date getArrivedAt() {
		return arrivedAt;
	}

	public void setArrivedAt(Date arrivedAt) {
		this.arrivedAt = arrivedAt;
	}
	
}
