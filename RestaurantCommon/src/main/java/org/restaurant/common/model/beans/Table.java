package org.restaurant.common.model.beans;

import java.util.List;

public class Table extends SimpleBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6843404536547273334L;
	private String name;
	private List<Chair> chairs;

	public String getName() {
		return name;
	}

	public void setName(String tableName) {
		this.name = tableName;
	}
	
	public List<Chair> getChairs() {
		return chairs;
	}

	public void setChairs(List<Chair> chairs) {
		this.chairs = chairs;
	}
}
