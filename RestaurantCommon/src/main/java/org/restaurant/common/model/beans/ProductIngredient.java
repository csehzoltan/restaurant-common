package org.restaurant.common.model.beans;

public class ProductIngredient extends SimpleBean {

	private static final long serialVersionUID = -2575417939309440898L;

	private Integer productId;
	private Integer ingredientId;
	private Double quantity;
	private Integer unitId;
	
	public ProductIngredient(Integer productId, Integer ingredientId, Double quantity, Integer unitId) {
		this.productId = productId;
		this.ingredientId = ingredientId;
		this.quantity = quantity;
		this.unitId = unitId;
	}
	
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(Integer ingredientId) {
		this.ingredientId = ingredientId;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

}
