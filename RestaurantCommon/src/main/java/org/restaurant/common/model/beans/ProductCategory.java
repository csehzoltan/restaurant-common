package org.restaurant.common.model.beans;

public enum ProductCategory {
	
	SOUP(1, "Soup", 1),
	APPETIZER(2, "Appetizer", 2),
	MAINCOURE(3, "Main course", 3),
	PASTA(4, "Pasta", 4),
	SALAD(5, "Salad", 5),
	DESSERT(6, "Dessert", 6),
	DRINK(7, "Drink", 7);
	
	private final int id;
	private final String name;
	private final Integer no;
	
	private ProductCategory(int id, String name, Integer no) {
		this.id = id;
		this.name = name;
		this.no = no;
	}

	public static ProductCategory getById(int id) {
		for (ProductCategory e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static ProductCategory getByName(String name) {
		for (ProductCategory e : values()) {
			if (e.name.equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}
	
	public Integer getNo() {
		return no;
	}
	
	public int getId() {
		return id;
	}
}
