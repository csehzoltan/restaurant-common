package org.restaurant.common.model.beans;

import java.util.Date;
import java.util.List;

public class User extends SimpleBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1171406294792415300L;
	
	private String name;
	private String accountName;
	private String password;
	Date birthDay;
	Date registrationDate;
	private String email;
	private String phoneNumber;
	private UserType type;
	private List<ProductRequest> productRequests;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public UserType getType() {
		return type;
	}
	public void setType(UserType type) {
		this.type = type;
	}
	
	public List<ProductRequest> getProductRequests() {
		return productRequests;
	}
	public void setProductRequests(List<ProductRequest> productionRequests) {
		this.productRequests = productionRequests;
	}
	
}
