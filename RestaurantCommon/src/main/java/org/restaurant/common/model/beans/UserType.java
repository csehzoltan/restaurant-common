package org.restaurant.common.model.beans;

public enum UserType {
	ADMINISTRATOR(1, "Administrator"), WAITER(2, "Waiter"), GUEST(3, "Guest");

	private int id;
	private String display;

	UserType(int id, String display) {
		this.id = id;
		this.display = display;
	}

	public static UserType getByValue(String display) {
		for (UserType e : values()) {
			if (e.display.equalsIgnoreCase(display)) {
				return e;
			}
		}
		return null;
	}

	public int getId() {
		return this.id;
	}

	public String getDisplay() {
		return display;
	}
}