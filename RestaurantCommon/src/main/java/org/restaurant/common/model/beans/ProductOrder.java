package org.restaurant.common.model.beans;

public class ProductOrder extends SimpleBean {

	private static final long serialVersionUID = 5574948105298497568L;

	private Integer productRequestId;
	private ProductPrice productPrice;
	private Chair chair;
	private Integer quantity;
	private int rejectedQuantity;
	private ProductOrderState state;

	public Integer getProductRequestId() {
		return productRequestId;
	}

	public void setProductRequestId(Integer productRequestId) {
		this.productRequestId = productRequestId;
	}

	public ProductPrice getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(ProductPrice productPrice) {
		this.productPrice = productPrice;
	}

	public Chair getChair() {
		return chair;
	}

	public void setChair(Chair chair) {
		this.chair = chair;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public int getRejectedQuantity() {
		return rejectedQuantity;
	}

	public void setRejectedQuantity(int rejectedQuantity) {
		this.rejectedQuantity = rejectedQuantity;
	}

	public ProductOrderState getState() {
		return state;
	}

	public void setState(ProductOrderState state) {
		this.state = state;
	}

}
