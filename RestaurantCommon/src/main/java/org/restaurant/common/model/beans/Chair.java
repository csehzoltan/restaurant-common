package org.restaurant.common.model.beans;

public class Chair extends SimpleBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 307912939526427670L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String chairName) {
		this.name = chairName;
	}
	
	
}
