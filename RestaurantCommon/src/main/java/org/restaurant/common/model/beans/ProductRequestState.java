package org.restaurant.common.model.beans;

public enum ProductRequestState {
	OPEN(1, "Open"), 
	CLOSED(2, "Closed"), 
	PAYING(3, "Paying");
	
	private int id;
	private String display;

	ProductRequestState(int id, String display) {
		this.id = id;
		this.display = display;
	}
	
	public static ProductRequestState getById(int id) {
		for (ProductRequestState e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}

	public static ProductRequestState getByValue(String display) {
		for (ProductRequestState e : values()) {
			if (e.display.equalsIgnoreCase(display)) {
				return e;
			}
		}
		return null;
	}

	public String getDisplay() {
		return display;
	}

	public int getId() {
		return id;
	}
}
