package org.restaurant.common.json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class JsonHelper {
	
	private JsonHelper() {
		
	}
	
	/**
	 * Create a JsonObject, when send message we need to have an action member it's create it as JsonObject.
	 * @param action action name
	 * @param data data
	 * @return JsonObject
	 */
	public static JsonObject jsonActonRequest(String action, String data) {
		JsonElement actionNameJsonElement = new JsonParser().parse(action);
		JsonObject objectMsg = new JsonObject();
		objectMsg.add("action", actionNameJsonElement);
		if (data != null) {
			JsonElement dataJsonElement = new JsonParser().parse(data);
			objectMsg.add("data", dataJsonElement);
		}
		return objectMsg;
	}
	
	/**
	 * A megadott data és info objektumokat és művelet nevet konvertálja json üzenetté.
	 * @param action Művelet neve.
	 * @param data Objektum amit Json-né szeretnénk parsolni.
	 * @param info info map amit Json-né szeretnénk parsolni, kulcs a művelet nevét tartalmazza értéke magát az értéket.
	 * @return Parsolt Json objektum Stringként.
	 */
	public static String createJsonMsg(String action, Object data, Map<String, String> info) {
		JsonObject sendMsg = new JsonObject();
		JsonElement actionJsonElement = new JsonParser().parse(action);
		sendMsg.add("action", actionJsonElement);

		Gson gson = new GsonBuilder().create();
		
		if(data != null) {
			String json = gson.toJson(data);
			sendMsg.add("data", new JsonParser().parse(json).getAsJsonObject());
		}

		if(info != null) {
			String jsonMapStr = gson.toJson(info);
			sendMsg.add("info", new JsonParser().parse(jsonMapStr));
		}

		return sendMsg.toString();
	}
	
	/**
	 * Json objektumot hoz létre, melybe beleteszi a művelet (action) nevét és egy listát.
	 * @param actionName művelet neve.
	 * @param objects lista.
	 * @param myClass lista elemeinek az osztály típusa.
	 * @return a létrehozott Json objektummal tér vissza.
	 */
	public static JsonObject createJsonMsgList(String action, List<?> objects, Class<?> myClass) {
		JsonObject sendMsg = new JsonObject();
		
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(objects, TypeToken.getParameterized(ArrayList.class, myClass).getType());

			sendMsg.add("action", new JsonParser().parse(action));
			sendMsg.add("data", obj);
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return sendMsg;	
	}
	
	public static JsonObject createJsonList(String action, List<?> objects) {
		
		JsonObject sendMsg = new JsonObject();
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(objects, new TypeToken<List<?>>() {}.getType());

			sendMsg.add("action", new JsonParser().parse(action));
			sendMsg.add("data", obj);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return sendMsg;
	}
	
}
