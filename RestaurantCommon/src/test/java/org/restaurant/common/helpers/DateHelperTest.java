package org.restaurant.common.helpers;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Locale;

import org.junit.Test;

public class DateHelperTest {

	@Test
	public void formatOK1() throws ParseException {
		String expectedDate = "2018-11-26";
		assertEquals(expectedDate, DateHelper.format("26-NOV-2018", "dd-MMM-yyyy", "yyyy-MM-dd", Locale.US));
	}
	
	@Test
	public void formatOK2() throws ParseException {
		String expectedDate = "2018-03-10 12:02:18";
		assertEquals(expectedDate, DateHelper.format("Sat Mar 10 12:02:18 CET 2018", "EEE MMM dd HH:mm:ss zzz yyyy", "yyyy-MM-dd HH:mm:ss", Locale.US));
	}
	
	@Test(expected=ParseException.class)
	public void formatWrong1() throws ParseException {
		DateHelper.format("26-11-2018", "dd-MMM-yyyy", "yyyy-MM-dd", Locale.US);
	}
	
	@Test
	public void formatWrong2() throws ParseException {
		String wrongDate = "2018-11-26";
		assertTrue(!wrongDate.equals(DateHelper.format("26-OCT-2016", "dd-MMM-yyyy", "yyyy-MM-dd", Locale.US)));
	}

}
