package org.restaurant.common.helpers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UnitHelperTest {
	@Test
	public void testVolumeConversion() throws UnitException {
		double epsilon = 0.0001;
		assertEquals(1.0, UnitHelper.unitConversion(100, "ml", "dl"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(0.01, "dl", "ml"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(10, "dl", "l"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(0.10, "l", "dl"), epsilon);
		assertEquals(1000, UnitHelper.unitConversion(1, "l", "ml"), epsilon);
		assertEquals(0.001, UnitHelper.unitConversion(1, "ml", "l"), epsilon);
	}
	
	@Test
	public void testMassConversion() throws UnitException {
		double epsilon = 0.0001;
		assertEquals(1.0, UnitHelper.unitConversion(1000, "mg", "g"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(1000000, "mg", "kg"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(0.001, "g", "mg"), epsilon);
		assertEquals(1.0, UnitHelper.unitConversion(1000, "g", "kg"), epsilon);
		assertEquals(1000, UnitHelper.unitConversion(1, "kg", "g"), epsilon);
		assertEquals(1000000.0, UnitHelper.unitConversion(1, "kg", "mg"), epsilon);
	}
	
	@Test(expected=UnitException.class)
	public void testUnitConversionWrongParameters() throws UnitException {	
		UnitHelper.unitConversion(10, "mg", "ml");
		UnitHelper.unitConversion(10, "ml", "mg");
		
		UnitHelper.unitConversion(10, "unknown", "g");
	}

}

